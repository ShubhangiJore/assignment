package com.dynamic.question.answer.dao;

import java.util.List;

import com.dynamic.question.answer.dto.DynamicQuestionMasterDto;
import com.dynamic.question.answer.dto.ResponseEntity;


public interface DynamicQuestionAddInterface {
	
	public List<DynamicQuestionMasterDto> getAllQuestionDaoMetho() throws Exception;
	 public ResponseEntity saveQuestionAnswerDaoMethod(List<DynamicQuestionMasterDto> dto) throws Exception;
	
	

}
