package com.dynamic.question.answer.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.dynamic.question.answer.dto.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dynamic.question.answer.dto.DynamicmasterDto;
import com.dynamic.question.answer.service.DynamicQuestionAnswerAddService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class QuestionAnsController {

	@Autowired
	DynamicQuestionAnswerAddService dynamicQuestionAddService; // it is
	
	ObjectMapper mapperObj = null;

	@GetMapping("/genrateTreeStructure")
	public String getAllQuestionAnswer() {
		Map<String, Object> questionAnswerMap = new HashMap<String, Object>();
		String jsonResp = null;
		mapperObj = new ObjectMapper();
		try {
			questionAnswerMap.put("DynamicmasterDto",
					dynamicQuestionAddService.getAllQuestionServiceMetho());

			jsonResp = mapperObj.writeValueAsString(questionAnswerMap);
		} catch (JsonGenerationException e) {

		} catch (JsonMappingException e) {
		} catch (IOException e) {
		} catch (Exception e) {

		}
		System.out.println("jsonResp" + jsonResp);
		return jsonResp;
	}

	@PostMapping(value = "/save")
	// post mapping for save all dynamically added questin & answer
	public String saveDynamicQuestionAnswerForm(DynamicmasterDto dynamicMsterDto) {
		ResponseEntity response = null;
		try {
			response = dynamicQuestionAddService
					.saveQuestionAnswerServiceMethod(dynamicMsterDto
							.getDynamicQuestionMasterDtoList());
			return response.getResponseMsg();
		} catch (Exception e) {
			response.setResponseMsg("Duw to internal Error we can't proceed");
			return response.getResponseMsg();
		}
	}

}
