package com.dynamic.question.answer.dto;

import java.util.List;

public class DynamicQuestionMasterDto {

	private Integer sno;
	private String questionSno;
	private String parentLevel;
	private String childLevel;
	private Integer typeOfQuestion;
	private String question;
	private String ans;
	private String selfReferenceId;
	private List<String> ansList;

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public String getQuestionSno() {
		return questionSno;
	}

	public void setQuestionSno(String questionSno) {
		this.questionSno = questionSno;
	}

	public String getParentLevel() {
		return parentLevel;
	}

	public void setParentLevel(String parentLevel) {
		this.parentLevel = parentLevel;
	}

	public String getChildLevel() {
		return childLevel;
	}

	public void setChildLevel(String childLevel) {
		this.childLevel = childLevel;
	}

	public Integer getTypeOfQuestion() {
		return typeOfQuestion;
	}

	public void setTypeOfQuestion(Integer typeOfQuestion) {
		this.typeOfQuestion = typeOfQuestion;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAns() {
		return ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public List<String> getAnsList() {
		return ansList;
	}

	public void setAnsList(List<String> ansList) {
		this.ansList = ansList;
	}

	public DynamicQuestionMasterDto() {
	}

	public String getSelfReferenceId() {
		return selfReferenceId;
	}

	public void setSelfReferenceId(String selfReferenceId) {
		this.selfReferenceId = selfReferenceId;
	}

	public DynamicQuestionMasterDto(String questionSno, String question,
			String ans, String selfReferenceId,Integer typeOfQuestion) {
		super();
		this.questionSno = questionSno;
		this.question = question;
		this.ans = ans;
		this.selfReferenceId = selfReferenceId;
		this.typeOfQuestion =typeOfQuestion;
	}

	@Override
	public String toString() {
		return "DynamicQuestionMasterDto [questionSno=" + questionSno
				+ ", parentLevel=" + parentLevel + ", childLevel=" + childLevel
				+ ", question=" + question + ", ans=" + ans
				+ ", selfReferenceId=" + selfReferenceId + ", ansList="
				+ ansList + "]";
	}

}