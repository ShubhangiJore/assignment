package com.dynamic.question.answer.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DataConnectionPool{

	static Connection conn = null;
	private static ResourceBundle rb = ResourceBundle.getBundle("DatabaseConection");

	public static Connection getConnection() {

		try {
			System.out.println(rb.getString("jdbc.driverClassName"));
			Class.forName(rb.getString("jdbc.driverClassName"));
			conn = DriverManager.getConnection(rb.getString("jdbc.url"),
					rb.getString("jdbc.username"),
					rb.getString("jdbc.password"));
			System.out.println("open a connection");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static void closeConnection() {

		if (conn != null) {
			try {
				conn.close();
				System.out.println("close a connection");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
