package com.dynamic.question.answer.dto;

public class DynamicAnswerChildDto {

	private Integer sno;
	private Integer noOfQuesion;
	private String answer;
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public Integer getNoOfQuesion() {
		return noOfQuesion;
	}
	public void setNoOfQuesion(Integer noOfQuesion) {
		this.noOfQuesion = noOfQuesion;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "DynamicAnswerChildDto [sno=" + sno + ", noOfQuesion="
				+ noOfQuesion + ", answer=" + answer + "]";
	}
	
	

}
