package com.dynamic.question.answer.dto;

public class ResponseDto {

private String responseMessage;
private int responseCode;

public String getResponseMessage() {
	return responseMessage;
}
public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
}
public int getResponseCode() {
	return responseCode;
}
public void setResponseCode(int responseCode) {
	this.responseCode = responseCode;
}
@Override
public String toString() {
	return "ResponseDto [responseMessage=" + responseMessage
			+ ", responseCode=" + responseCode + "]";
}



}
