package com.dynamic.question.answer.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.dynamic.question.answer.dto.DynamicQuestionMasterDto;
import com.dynamic.question.answer.dto.ResponseEntity;

public class DynamicQuestionDaoService implements DynamicQuestionAddInterface {

	ResultSet rs = null;
	PreparedStatement preparedStatement = null;
	Connection connection = null;
	String sql = "";
	Statement statment = null;

	public List<DynamicQuestionMasterDto> getAllQuestionDaoMetho()
			throws Exception {
		connection = MysqlCon.getConnection();
		List<DynamicQuestionMasterDto> dynamicQuestionMasterDtoList = new ArrayList<DynamicQuestionMasterDto>();
		try {
			statment = connection.createStatement();
			String sql = "select  question.questionSno as questionSequence,question.question as question,answer.answer as answer,answer.snoOfQuestionMaster as masterId,SUBSTRING(parentlevel,1,(LENGTH(parentlevel)-2)) as parentId,question.typeOfQuestion as typeOfQuestion from question_master AS question, answer_master as answer where question.sno=answer.snoOfQuestionMaster";
			ResultSet rs = statment.executeQuery(sql);
			while (rs.next()) {
				String questionSequence = rs.getString("questionSequence");
				String question = rs.getString("question");
				String answer = rs.getString("answer");
				String parentId = rs.getString("parentId");
				Integer typeOfQuestion = rs.getInt("typeOfQuestion");
				DynamicQuestionMasterDto d = new DynamicQuestionMasterDto(
						questionSequence, question, answer, parentId,
						typeOfQuestion);
				dynamicQuestionMasterDtoList.add(d);
			}
		} catch (SQLException e) {
			// Handle errors for JDBC
			System.out.println(e);

		} catch (Exception e) {
			throw e;
		} finally {
			// clean up enviorment
			if (null != statment)
				statment.close();
			if (null != connection)
				connection.close();
		}
		return dynamicQuestionMasterDtoList;
	}
	public ResponseEntity saveQuestionAnswerDaoMethod(
			List<DynamicQuestionMasterDto> dtoList) throws Exception {
		ResponseEntity res = new ResponseEntity();
		String insertQuestionMaster = "INSERT INTO question_master"
				+ "(questionSno,typeOfQuestion,question,parentLevel) VALUES"
				+ "(?,?,?,?)";

		String insertAnswerDetail = "INSERT INTO answer_master"
				+ "(snoOfQuestionMaster,answer) VALUES" + "(?,?)";

		connection = MysqlCon.getConnection();
		statment = connection.createStatement();
		statment.executeUpdate("TRUNCATE TABLE answer_master");
		statment.executeUpdate("TRUNCATE TABLE question_master");

		for (DynamicQuestionMasterDto dynamicMasterDto : dtoList) {
			try {
				connection = MysqlCon.getConnection();

				preparedStatement = connection.prepareStatement(
						insertQuestionMaster, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(1, dynamicMasterDto
						.getQuestionSno().replaceAll("[+']", ""));
				preparedStatement.setInt(2,
						dynamicMasterDto.getTypeOfQuestion());
				preparedStatement.setString(3, dynamicMasterDto.getQuestion());
				preparedStatement.setString(4, dynamicMasterDto
						.getParentLevel().replaceAll("[+']", ""));

				preparedStatement.executeUpdate();
				ResultSet rs = preparedStatement.getGeneratedKeys();
				if (rs != null && rs.next()) {
					for (String ans : dynamicMasterDto.getAnsList()) {
						if (null != ans && !ans.isEmpty()) {
							PreparedStatement preparedStatement1 = connection
									.prepareStatement(insertAnswerDetail);
							preparedStatement1.setInt(1, rs.getInt(1));
							preparedStatement1.setString(2, ans);
							preparedStatement1.executeUpdate();

						}
					}

				}
				// connection.commit();
			} catch (Exception e) {
				throw e;
			} finally {
				// finally block used to close resources
				try {
					// close connection
					if (null != preparedStatement)
						preparedStatement.close();
					if (null != connection)
						connection.close();
				} catch (Exception se2) {
				}// nothing we can do
					// end finally try }
			}
			res.setResponseMsg("Record created...............");
		}
		return res;
	}

}
