package com.dynamic.question.answer.dto;

public class ResponseEntity {

	private String ResponseMsg;
	private String ResponseCode;
	public String getResponseMsg() {
		return ResponseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		ResponseMsg = responseMsg;
	}
	public String getResponseCode() {
		return ResponseCode;
	}
	public void setResponseCode(String responseCode) {
		ResponseCode = responseCode;
	}
	
	
	
}
