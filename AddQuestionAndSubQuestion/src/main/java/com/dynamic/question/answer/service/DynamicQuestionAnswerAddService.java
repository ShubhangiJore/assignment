package com.dynamic.question.answer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.dynamic.question.answer.dao.DynamicQuestionDaoService;
import com.dynamic.question.answer.dto.DynamicQuestionMasterDto;
import com.dynamic.question.answer.dto.ResponseEntity;



public class DynamicQuestionAnswerAddService implements
		DynamicQuestionAddServiceInterFace {

	@Autowired
	DynamicQuestionDaoService dynamicQuestionAddDao; // it is responsible to add
														// recusively question
														// and answer & show
														// tress structure

	public List<DynamicQuestionMasterDto> getAllQuestionServiceMetho()
			throws Exception {
		
		return dynamicQuestionAddDao.getAllQuestionDaoMetho();
	}

	public ResponseEntity saveQuestionAnswerServiceMethod(
			List<DynamicQuestionMasterDto> dtoList) throws Exception {

		return dynamicQuestionAddDao.saveQuestionAnswerDaoMethod(dtoList);

	}

}
