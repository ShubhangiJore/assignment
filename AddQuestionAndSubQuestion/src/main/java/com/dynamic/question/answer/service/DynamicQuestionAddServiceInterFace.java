package com.dynamic.question.answer.service;

import java.util.List;

import com.dynamic.question.answer.dto.DynamicQuestionMasterDto;
import com.dynamic.question.answer.dto.ResponseEntity;

public interface DynamicQuestionAddServiceInterFace {
	
	 public List<DynamicQuestionMasterDto> getAllQuestionServiceMetho() throws Exception;
	 public ResponseEntity saveQuestionAnswerServiceMethod(List<DynamicQuestionMasterDto> dto) throws Exception;
}
