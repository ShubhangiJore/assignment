package com.dynamic.question.answer.dto;

import java.util.List;

public class DynamicmasterDto {

	private List<DynamicQuestionMasterDto> dynamicQuestionMasterDtoList;

	public List<DynamicQuestionMasterDto> getDynamicQuestionMasterDtoList() {
		return dynamicQuestionMasterDtoList;
	}

	public void setDynamicQuestionMasterDtoList(
			List<DynamicQuestionMasterDto> dynamicQuestionMasterDtoList) {
		this.dynamicQuestionMasterDtoList = dynamicQuestionMasterDtoList;
	}

	@Override
	public String toString() {
		return "DynamicmasterDto [dynamicQuestionMasterDtoList="
				+ dynamicQuestionMasterDtoList + "]";
	}
	
}
