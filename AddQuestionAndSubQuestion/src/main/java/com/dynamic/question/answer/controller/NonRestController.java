package com.dynamic.question.answer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NonRestController {

	@RequestMapping("/AddQuestion")
	public String getQuestionAnsForm() {
		return "addQuestionAnswerForm"; // it show that request mapping render to particular jsp there is no need to show through model and view 
	}

	@RequestMapping("/showTreeStructure")
	public String getTreeStructure() {
		return "treeStructureQuestionAnswer";// it show that request mapping render to particular jsp
	}
	
}