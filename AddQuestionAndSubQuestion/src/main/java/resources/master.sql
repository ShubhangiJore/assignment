/*
SQLyog Ultimate v8.55 
MySQL - 5.5.47-0ubuntu0.14.04.1 : Database - dynamic_add_questin_anser_5.3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dynamic_add_questin_anser_5.3` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dynamic_add_questin_anser_5.3`;

/*Table structure for table `answer_master` */

DROP TABLE IF EXISTS `answer_master`;

CREATE TABLE `answer_master` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `snoOfQuestionMaster` int(11) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `answer_master` */

/*Table structure for table `question_master` */

DROP TABLE IF EXISTS `question_master`;

CREATE TABLE `question_master` (
  `sno` int(11) NOT NULL AUTO_INCREMENT,
  `questionSno` varchar(255) DEFAULT NULL,
  `typeOfQuestion` tinyint(1) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `parentLevel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `question_master` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
