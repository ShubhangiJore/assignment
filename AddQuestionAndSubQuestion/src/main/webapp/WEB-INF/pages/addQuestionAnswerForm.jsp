<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!doctype html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<title>Dynamic Question</title>
<link rel="stylesheet" href="resources/customcss/questionAnswerForm.css">
<!-- Latest compiled and minified CSS -->


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="resources/customjs/dynamicQuestionAnswerForm.js"
	type="text/javascript"></script>
<script src="resources/jQuery/jquery.js" type="text/javascript"></script>

</head>
<script type="text/javascript">
	$(document)
			.ready(
					function() {

						$("#getJsonObject").click(function() {
							saveQuestionAnswerHierachy();
						});

						$("#getTreeStructure")
								.click(
										function() {

											$.ajax({
												type : "GET",
												url : "genrateTreeStructure",
												async : false,
												success : function(data) {
													masterObject = jQuery
															.parseJSON(data);
													makeDivEmpty();
													genRateTree(masterObject);

												}
											});

											function genRateTree(masterObject) {
												$
														.each(
																masterObject.DynamicmasterDto,
																function(key,
																		value) {

																	if ('' == value.selfReferenceId) {
																		$(
																				'#appendQuestionAnswerDiv')
																				.append(
																						'<ul id="ul_'+key+'"><span class="question">Question : </span><span class="tab"><font color:white>'
																								+ value.question
																								+ '</font></span><li style="list-style: none;" class=""><span class="answer">Answer : </span><span class="tab">'
																								+ value.ans
																								+ '</span></li></ul>');
																		getChildInformation(
																				value.questionSno,
																				key);

																	}
																});
											}
											function getChildInformation(
													question, count) {
												$
														.each(
																masterObject.DynamicmasterDto,
																function(key,
																		value) {
																	if (question == value.selfReferenceId) {
																			$('#appendQuestionAnswerDiv')
																					.append(
																							'<ul class="child cul_'+count+'" id="ul_'+count+'"><span class="question">Question : </span><span class="tab"><font color:white>'
																									+ value.question
																									+ '</font></span><li   style="list-style: none;" class="ul_'+count+'"><span class="answer">Answer : </span><span class="tab">'
																									+ value.ans
																									+ '</span></li></ul>');

																		getChildInformation(
																				value.questionSno,
																				count)
																	}
																});
											}

										});

						$('.level_1').on('click', spawn);
						function spawn() {
							// check level
							var level = stripLevel(this.className);
							if (level !== '') {
								var countOthers = this.parentNode
										.querySelectorAll("[class^='level_"
												+ level + "']").length;
								console.log('countOthers' + countOthers);
								var x = wrapElement(level, countOthers);
								if (level.length == 1) {
									$('#appendQuestionAnswerDiv').append(x);
								} else {
									//x.insertAfter(this);
									$(this).parent().append(x);
								}
							}
						}

						// strip level
						var stripLevel = function(className) {
							var index = className.indexOf('_');
							if (index > -1) {
								return className.substr(index + 1);
							} else {
								return '';
							}
						}
						// wrapper element
						var wrapElement = function(level, number) {
							var genratedId = level + number;
							console.log(genratedId);

							var id = level + '-' + number;
							var newChildId = "" + id;

							var dynamicDivId = "d" + genratedId;
							var numItems = $('.questionTextBox').length;
							if (level.length == 1) {
								// it's parent
								var numItems = $('.questionTextBox').length;

								var genratedId = level + number;

								var id = level + '-' + number;
								var div = $('<div class="test" id='+genratedId+'></div>');
								var input = $('<input type="text" placeholder="Enter Question" name="dynamicQuestionMasterDtoList['+numItems+'].question" class="questionTextBox" />');
								var dropdown = $('<select class="dropdown" id="d'
										+ genratedId
										+ '" name="dynamicQuestionMasterDtoList['
										+ numItems
										+ '].typeOfQuestion" onChange="checkit('
										+ genratedId
										+ ','
										+ numItems
										+ ');"><option value=""></option><option value="0">Single Choice</option><option value="1">Multi-line text</option><option value="2">Multi Choice</option></select>');

								div.append(input);
								div.append(dropdown);
								var hiddenInputParentLevel = $('<input type="hidden"  name="dynamicQuestionMasterDtoList['+numItems+'].parentLevel" value='+level+' class="" />');
								var hiddenInputParentLevel2 = $('<input type="hidden"  name="dynamicQuestionMasterDtoList['+numItems+'].questionSno" value="\''+ id+ '\'" />');
								div.append(hiddenInputParentLevel);
								div.append(hiddenInputParentLevel2);

							} else {
								// it's child
								//var id =  +level+ + '-' + "+number+";

								var id = level + '-' + number;
								var newChildId = "" + id;
								var div = $('<div class="test" id='+newChildId+'></div>');
								alert("wrapElement-->shu" + id);
								var input = $('<input type="text" name="dynamicQuestionMasterDtoList['+numItems+'].question"  placeholder="Enter Sub Question"  id='+newChildId +'  class="questionTextBox"/>');
								var dropdown = $('<select class="dropdown" name="dynamicQuestionMasterDtoList['
										+ numItems
										+ '].typeOfQuestion" id="d'
										+ id
										+ '" onChange ="checkit(\''
										+ newChildId
										+ '\','
										+ numItems
										+ ');"><option value=""></option><option value="0">Single Choice</option><option value="1">Multi-line text</option><option value="2">Multi Choice</option></select>');

								div.append(input);
								div.append(dropdown);
								var hiddenInputParentLevel = $('<input type="hidden"  name="dynamicQuestionMasterDtoList['+numItems+'].parentLevel" value="\''+ newChildId+ '\'" />');
								var hiddenInputParentLevel2 = $('<input type="hidden"  name="dynamicQuestionMasterDtoList['+numItems+'].questionSno" value="\''+ newChildId+ '\'" />');
								div.append(hiddenInputParentLevel);
								div.append(hiddenInputParentLevel2);

							}
							// add button

							var button = $('<input class="level_' + level + '-' + number + '"  type="button" class="test" value="Add Sub Question" />');
							button.on('click', spawn);
							div.append(button);
							div.css('margin-left', (level.length * 10) + 'px');
							return div;
						}

					});
</script>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">
	<div class="container">
		<div>
			<p class="text-info lead">
				<b>ADD NEW CALL</b>
			<hr>
		</div>
		<div
			style="height: 500px; width: 1000px; border: 1px background; color: black;">
			<form id="formData" action="save">
				<div id="appendQuestionAnswerDiv"
					style="height: 500px; width: 750px; background-color: navy; overflow-y: scroll; overflow-x: scroll">
					<input class="level_1" type="button" value="Add Question" />
				</div>
				<div class="pull-right">
					<!-- 	<input class="level_1" style="bottom: 0; position: absolute;"
						type="button" value="Add Question" /> -->
				</div>

				<div align="center">
					<button type="button" style="" class="btn btn-primary active"
						id="getJsonObject">Save</button>
					<button type="button" id="getTreeStructure"
						class="btn btn-primary active">Generate Tree Structure</button>
					<button type="button" onclick="makeDivEmpty()"
						class="btn btn-default">Cancel</button>
				</div>
			</form>
		</div>
	</div>
	<!-- version information  bootstrap: 3.3.7,
	juery.min:3.2.1 -->
	<script src="resources/jQuery/jquery.min.js" type="text/javascript"></script>
	<script src="resources/jQuery/jquery.toObject.js"
		type="text/javascript"></script>
	<script src="resources/jQuery/form2js.js" type="text/javascript"></script>


</body>
</html>